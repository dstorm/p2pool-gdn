import os
import platform

from twisted.internet import defer

from . import data
from p2pool.util import math, pack, jsonrpc
from operator import *

def get_gdnsubsidy(bnHeight):
    if bnHeight == 1:
        nSubsidy = 28000
    elif bnHeight <= 10:
        nSubsidy = 1
    else:
        nSubsidy = 40
    return int(nSubsidy * 100000000)

@defer.inlineCallbacks
def check_genesis_block(bitcoind, genesis_block_hash):
    try:
        yield bitcoind.rpc_getblock(genesis_block_hash)
    except jsonrpc.Error_for_code(-5):
        defer.returnValue(False)
    else:
        defer.returnValue(True)

nets = dict(

    globaldenomination=math.Object(
        P2P_PREFIX='fec3b9de'.decode('hex'),
        P2P_PORT=38348,
        ADDRESS_VERSION=38,
        RPC_PORT=38347,
        RPC_CHECK=defer.inlineCallbacks(lambda bitcoind: defer.returnValue(
            'GlobalDenominationaddress' in (yield bitcoind.rpc_help()) and
            not (yield bitcoind.rpc_getinfo())['testnet']
        )),                           
        SUBSIDY_FUNC=lambda nBits, height: get_gdnsubsidy(height+1),
        BLOCKHASH_FUNC=lambda data: pack.IntType(256).unpack(__import__('xcoin_hash').getPoWHash(data)),
        POW_FUNC=lambda data: pack.IntType(256).unpack(__import__('xcoin_hash').getPoWHash(data)),
        BLOCK_PERIOD=60, # s
        SYMBOL='GDN',
        CONF_FILE_FUNC=lambda: os.path.join(os.path.join(os.environ['APPDATA'], 'GlobalDenomination') if platform.system() == 'Windows' else os.path.expanduser('~/Library/Application Support/GlobalDenomination/') if platform.system() == 'Darwin' else os.path.expanduser('~/.GlobalDenomination'), 'GlobalDenomination.conf'),
        BLOCK_EXPLORER_URL_PREFIX='',
        ADDRESS_EXPLORER_URL_PREFIX='',
        TX_EXPLORER_URL_PREFIX='',
        SANE_TARGET_RANGE=(2**256//2**32//1000 - 1, 2**256//2**20 - 1), 
        DUMB_SCRYPT_DIFF=1,
        DUST_THRESHOLD=0.001e8,
    ),

)
for net_name, net in nets.iteritems():
    net.NAME = net_name
